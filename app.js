'use strict';

require('dotenv').load();
const express        = require('express');
const cors           = require('cors');
const compression    = require('compression');
const bodyParser     = require('body-parser');
const helmet         = require('helmet');
const expressWinston = require('express-winston');
const swaggerTools   = require('swagger-tools');

const db = require('./lib/cache-db');


function initialize(logger) {
    const app = express();

    if (process.env.SMART_PARKING_API_IS_BEHIND_PROXY) {
        // http://expressjs.com/api.html#trust.proxy.options.table
        app.enable('trust proxy');
    }

    app.use(helmet.hidePoweredBy());
    app.use(helmet.ieNoOpen());
    app.use(helmet.noSniff());
    app.use(helmet.frameguard());
    app.use(helmet.xssFilter());
    app.use(compression());
    app.use(cors());
    app.use(bodyParser.urlencoded({
        extended: true
    }));
    app.use(bodyParser.json());
    app.use(expressWinston.logger({
        winstonInstance: logger,
        expressFormat:   true,
        colorize:        false,
        meta:            false,
        statusLevels:    true
    }));

    const swaggerDoc     = require('./swagger/swagger.json');


    //Initialize db
    let stock_data = {
        tea: {
            type: "common",
            last_dividend: 0,
            par_value: 100
        },
        pop: {
            type: "common",
            last_dividend: 8,
            par_value: 100
        },
        ale: {
            type: "common",
            last_dividend: 23,
            par_value: 60
        },
        gin: {
            type: "preferred",
            last_dividend: 8,
            fixed_dividend: 0.02,
            par_value: 100
        },
        joe: {
            type: "common",
            last_dividend: 13,
            par_value: 250
        }
    }

    db.store("stock_data", stock_data);
    db.store("trades", []);



    return new Promise(function(resolve) {
        swaggerTools.initializeMiddleware(swaggerDoc, function(middleware) {
            app.use(middleware.swaggerMetadata());

            app.use(middleware.swaggerValidator());

            app.use(middleware.swaggerUi());

            //Route validated requests to appropriate controller
            app.use(middleware.swaggerRouter({
                controllers:           './lib/routes',
                ignoreMissingHandlers: true,
                useStubs:              false // Conditionally turn on stubs (mock mode)
            }));

            app.use(expressWinston.errorLogger({
                winstonInstance: logger
            }));

            app.use(function(err, req, res, next) {
                if (res.headersSent) {
                    return next(err);
                }

                res.status(err.status || 500);

                const error = {
                    errorCode:   res.statusCode,
                    userMessage: err.message
                };

                if (process.env.NODE_ENV === 'development') {
                    error.stack = err;
                }

                return res.json(error);
            });

            app.use(function(req, res) {
                res.status(404).json({
                    errorCode:   404,
                    userMessage: 'Not found.'
                });
            });

            resolve(app);
        });
    });
}

module.exports = {
    initialize
};
