Super Simple Stock Market

Install

    git clone git@gitlab.com:cocheok/super-simple-stock-market.git
    cd super-simple-stock-market
    npm install
    npm start
 

Run in docker

Build

    git clone git@gitlab.com:cocheok/super-simple-stock-market.git
    cd super-simple-stock-market
    docker build -t super-simple-stock-market .
  
Run
  
     docker run super-simple-stock-market -v JPMORGAN_SUPERSIMPLE_STOCK_MARKET_SERVER_PORT=3000 -v JPMORGAN_SUPERSIMPLE_STOCK_MARKET_SERVER_HOST=localhost

Run Docs

    mkdocs serve

Execute

1- For a given Stock

    i. Given any price as input, calculate the dividend yield

        curl -X GET "http://localhost:3000/api/dividend_yield/?stock_symbol=gin&price=4" -H "accept: application/json"

    ii. Given any price as input,  calculate the P/E Ratio

        curl -X GET "http://localhost:3000/api/p_e_ratio/?stock_symbol=gin&price=41" -H "accept: application/json"

    iii. Record a trade, with timestamp, quantity, buy or sell indicator and price


        curl -H "Content-Type: application/json" -X PUT -d '{ "stock_symbol": "gin", "quantity": 4,   "operation": "buy", "price": 110  }' http://localhost:3000/api/trade/

    iv. Calculate Volume Weighted Stock Price based on trades in past  5 minutes
    
    
        curl -X GET "http://jberrettamoreno.com/api/volume_weight/" -H "accept: application/json"

        or you can specify a number last_minutes, by default it's 5 minutes.

        curl -X GET "http://jberrettamoreno.com/api/volume_weight/?last_minutes=4" -H "accept: application/json"



2- Calculate the GBCE All Share Index using the geometric mean of the Volume Weighted Stock Price for all stocks
   
        curl -X GET "http://localhost:3000/api/gbce/" -H "accept: application/json"
   


