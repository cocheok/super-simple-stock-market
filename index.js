'use strict';

require('dotenv').load();

const app    = require('./app');
const logger = require('./lib/logger');

app.initialize(logger)
    .then((application) => {
        application.listen(process.env.JPMORGAN_SUPERSIMPLE_STOCK_MARKET_SERVER_PORT);
        logger.info('Your server is listening on port ' + process.env.JPMORGAN_SUPERSIMPLE_STOCK_MARKET_SERVER_PORT);
    })
    .catch(logger.error);
