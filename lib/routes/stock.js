'use strict';

const stock = require('../stock');


function getDividendYield(req, res) {

    let stockSymbol = req.swagger.params.stock_symbol.value;
    let price = req.swagger.params.price.value;
    try {
        res.json({calculated_value: stock.getDividendYield(stockSymbol, price)});
    } catch(err){
        res.status(500).send(err.message);
    }
}
function getPERatio(req, res) {
    let stockSymbol = req.swagger.params.stock_symbol.value;
    let price = req.swagger.params.price.value;
    try{
        res.json({calculated_value: stock.getPERatio(stockSymbol, price)});
    } catch(err){
        res.status(500).send(err.message);
    }
}
function setTrade(req, res) {
    let trade = req.swagger.params.body.value;

    try{
        stock.setTrade(trade);
        res.sendStatus(200);
    } catch(err){
        res.status(500).send(err.message);
    }
}
function getVolumeWeight(req, res) {
    let minutes = req.swagger.params.minutes.value;
    try{
        res.json({calculated_value: stock.getVolumeWeighted(minutes)});
    } catch(err){
        res.status(500).send(err.message);
    }
}
function getGBCE(req, res) {
    try{
        res.json({calculated_value: stock.getGBCE()});
    } catch(err){
        res.status(500).send(err.message);
    }
}


module.exports = {
    getDividendYield,
    getPERatio,
    setTrade,
    getVolumeWeight,
    getGBCE
};
