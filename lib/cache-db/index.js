'use strict';
const NodeCache = require('node-cache');
const myCache = new NodeCache();

function store(key, obj) {
    return myCache.set(key, obj);
}
function find(key) {
    return myCache.get(key);

}

module.exports = {
    store,
    find
};
