'use strict';
const cacheDB = require('../cache-db');
const moment = require('moment');
const roundTo = require('round-to');
function getDividendYield(stockSymbol, price) {

    if(!price) {
        throw new Error('Price is required');
    }

    let stockData = cacheDB.find('stock_data');

    if(!stockData) {
        throw new Error('Can\'t find stock symbol in database');
    }

    let result;
    if (stockData[stockSymbol].type === 'common') {
        result = stockData[stockSymbol].last_dividend / price;
    } else if(stockData[stockSymbol].type === 'preferred') {
        result = (stockData[stockSymbol].fixed_dividend * stockData[stockSymbol].par_value) / price;
    }
    return roundTo(result, 2);
}
function getPERatio(stockSymbol, price) {
    if(!stockSymbol) {
        throw new Error('Stock symbol is required');
    }
    if(!price) {
        throw new Error('Price is required');
    }
    let stockData = cacheDB.find('stock_data');
    if (!stockData) {
        throw new Error('Can\'t find stock symbol in database');
    }
    if (stockData[stockSymbol].last_dividend === 0) {
        throw new Error('Can\'t calculate P/E Ratio because last dividend of ' + stockSymbol + 'is 0.');
    }

    return roundTo(price / stockData[stockSymbol].last_dividend, 2);

}
function setTrade(trade) {

    if (trade.timestamp) {
        if(moment(trade.timestamp).isValid()){
            trade.timestamp = moment(trade.timestamp).format();
        }else {
            throw new Error('The timestamp format is invalid, must be YYYY-MM-DDTHH:mm:ss.SSSZ');
        }
    } else {
        trade.timestamp = moment().format();
    }
    let trades = cacheDB.find('trades');
    trades.push(trade);

    cacheDB.store('trades', trades);

}
function getVolumeWeighted(lastMinutes) {
    let minutes;
    if(lastMinutes) {
        minutes = lastMinutes;
    }else {
        minutes = 5;
    }
    let now = moment();

    let minutesAgo = moment().subtract({minutes: minutes});
    let trades = cacheDB.find('trades');
    let tradePriceQuantity = 0;
    let tradeQuantity = 0;


    trades.forEach(function(trade) {
        if (moment(trade.timestamp).isBetween(minutesAgo, now)) {
            tradePriceQuantity += (trade.price * trade.quantity);
            tradeQuantity += trade.quantity;
        }
    });

    if(tradeQuantity === 0){
        throw new Error('Can\'t calculate getVolumeWeighted because the trade Quantity is 0.');
    }
    return roundTo(tradePriceQuantity / tradeQuantity, 2);

}
function getGBCE() {
    let trades = cacheDB.find('trades');
    let stockData = cacheDB.find('stock_data');
    let vwp = {};
    trades.forEach(function(trade) {
        if (stockData[trade.stock_symbol]) {

            if (vwp[trade.stock_symbol]) {
                vwp[trade.stock_symbol].trade_price_quantity += (trade.price * trade.quantity);
                vwp[trade.stock_symbol].trade_quantity += trade.quantity;
            } else {
                vwp[trade.stock_symbol] = {
                    trade_price_quantity: (trade.price * trade.quantity),
                    trade_quantity: trade.quantity
                };
            }
        }

    });
    let prices = 0;
    let quantity = 0;
    for (var k in vwp) {
        if (prices === 0) {
            prices = (vwp[k].trade_price_quantity / vwp[k].trade_quantity);
        } else {
            prices *= (vwp[k].trade_price_quantity / vwp[k].trade_quantity);
        }
        quantity++;
    }
    if(quantity === 0){
        throw new Error('Can\'t calculate GBCE because Quantity is 0.');
    }
    return roundTo(Math.pow(prices, (1 / quantity)), 2);

}


module.exports = {
    getDividendYield,
    getPERatio,
    setTrade,
    getVolumeWeighted,
    getGBCE
};
