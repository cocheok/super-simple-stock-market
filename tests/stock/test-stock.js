'use strict';

const should = require('should');
const proxyquire = require('proxyquire').noCallThru();
const stock = require('../../lib/stock');
const cacheDB = require('../../lib/cache-db');
const moment = require('moment');
var sinon = require('sinon');

describe('Stock', () => {
    let cacheDBFindStub;
    let cacheDBStoreStub;
    describe('in develop', () => {
        cacheDBFindStub = sinon.stub(cacheDB, 'find', function (value) {
            if (value === "trades") {
                return [{
                    "stock_symbol": "gin",
                    "timestamp": moment().format(),
                    "quantity": 10,
                    "operation": "buy",
                    "price": 100
                    },
                    {
                        "stock_symbol": "gin",
                        "timestamp": moment().format(),
                        "quantity": 105,
                        "operation": "buy",
                        "price": 100
                    },
                    {
                        "stock_symbol": "ale",
                        "timestamp": moment().format(),
                        "quantity": 10,
                        "operation": "buy",
                        "price": 5
                    }
                ]
            }

            return {
                tea: {
                    type: "common",
                    last_dividend: 0,
                    par_value: 100
                },
                pop: {
                    type: "common",
                    last_dividend: 8,
                    par_value: 100
                },
                ale: {
                    type: "common",
                    last_dividend: 23,
                    par_value: 60
                },
                gin: {
                    type: "preferred",
                    last_dividend: 8,
                    fixed_dividend: 0.02,
                    par_value: 100
                },
                joe: {
                    type: "common",
                    last_dividend: 13,
                    par_value: 250
                }
            }


        });
        cacheDBStoreStub = sinon.stub(cacheDB, 'store');


        describe('happy_path', () => {

            it('getDividendYield', () => {

                stock.getDividendYield('gin', 4).should.be.equal(0.5);
            });
            it('getPERatio', () => {

                stock.getPERatio('gin', 4).should.be.equal(0.5);
            });
            it('getVolumeWeighted', () => {
                stock.getVolumeWeighted().should.be.equal(92.4);
            });
            it('getGBCE', () => {
                stock.getGBCE().should.be.equal(22.36);
            });

        });


    });


});
