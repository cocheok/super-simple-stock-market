# jpmorgan-supersimple-stock-market

##Install:
 ´´´
  git clone git@gitlab.com:cocheok/super-simple-stock-market.git
  cd super-simple-stock-market
  npm install
  npm start
 ´´´

## Run in docker
###Build
 ´´´
   git clone git@gitlab.com:cocheok/super-simple-stock-market.git
   cd super-simple-stock-market
   docker build -t super-simple-stock-market .
  ´´´
###Run
  ´´´
     docker run super-simple-stock-market -v JPMORGAN_SUPERSIMPLE_STOCK_MARKET_SERVER_PORT=3000 -v JPMORGAN_SUPERSIMPLE_STOCK_MARKET_SERVER_HOST=localhost

  ´´´

